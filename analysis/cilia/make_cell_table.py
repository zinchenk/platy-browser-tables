import pandas as pd

# more cilia merges:
{50: 13, 51: 42, 65: 19}

# ids from version 0.6.1
right_nephr_ids = [22545, 22546, 22549, 22214, 22211, 22541, 23199, 25143]
left_nephr_ids = [23132, 22827, 23296, 23295, 22826, 23531, 24723, 25845]
